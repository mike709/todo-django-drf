# ToDo application

## Description
Simple todo back-end application based on Django, DRF and SQlite DB.

## Installations
```bash
    pip install django
    pip install djangorestframework
    pip install django-cors-headers
```

## Start app
```bash
    python manage.py migrate #for first start
    python manage.py runserver
```
Server will be started at 8000 port by default.

## Tests
```bash
    python manage.py test
```